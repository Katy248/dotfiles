function New-Directory {
	param (
		[string]$Path
	)
	Write-Debug "Start ensuring that directory '$Path' exist"
	if (-not (Test-Path -Path $Path)) {
		Write-Debug "Directory '$Path' not exist"
		Write-Information "Creating directory '$Path'"
		New-Item -ItemType Directory -Path $Path
	}
	Write-Debug "Directory '$Path' exist"
}

function New-FileLink {
	param(
		[System.IO.FileInfo]$file,
		[string]$destinationDirectoryPath,
		[switch]$Force
	)
	Write-Information "Creating link for file '$($file.FullName)' to '$destinationDirectoryPath'"
		
	$itemDestinationPath = "$destinationPath/$($file.Name)"
	Write-Debug "Link destination Path is '$itemDestinationPath'"
	
	if (Test-Path $itemDestinationPath) {
		Write-Information "File '$itemDestinationPath' already exist"
		
		if (-not $Force) { 
			return
		}
		Write-Warning "Remove file '$itemDestinationPath'"
		Remove-Item -Path $itemDestinationPath
	}
	if ($IsLinux) {
		Write-Debug "Current OS is linux so using 'ln' for soft link"
		ln -s $file.FullName $destinationDirectoryPath
		Write-Information "Created link for file '$($file.FullName)' to '$destinationDirectoryPath'"
	}
	elseif ($IsWindows) {
		Write-Debug "Current OS is windows so using 'Copy-Item' for symbolik link"
		Copy-Item -Destination $itemDestinationPath -Path $file.FullName -Force:$Force
	}
}


<#
.SYNOPSIS
    Creates soft copy of item.
.DESCRIPTION
    A longer description of the function, its purpose, common use cases, etc.
.NOTES
    Information or caveats about the function e.g. 'This function is not supported in Linux'
.LINK
    Specify a URI to a help page, this will show when Get-Help -Online is used.
.EXAMPLE
    New-Link -TargetPath "targetDir" -DestinationPath "destDir"
    New-Link "targetDir" "destDir"
    New-Link "targetDir" "destDir" -Force
#>


function New-Link {
	[CmdletBinding()]
	param(
		[string]$TargetDirectory,
		[string]$DestinationPath,
		[switch]$Force
	)

	Begin {
		if (-not (Test-Path -Path $TargetDirectory)) {
			Write-Error "Cannot copy directory '$TargetDirectory' because it doesn't exist" -Category InvalidArgument -ErrorAction Continue
			break
		}
	}

	Process {
		Write-Information "Copy directory '$TargetDirectory' to '$DestinationPath'"

		New-Directory $DestinationPath

		$items = Get-ChildItem $TargetDirectory 

		foreach ($i in $items) {

			$itemDestinationPath = "$DestinationPath/$($i.Name)"

			$item = Get-Item $i

			if ($item -is [System.IO.DirectoryInfo]) {
				New-Link $i $itemDestinationPath
			}
			elseif ($item -is [System.IO.FileInfo]) {
				New-FileLink $item $DestinationPath -Force $Force
			}
			else {
				Write-Warning "Unknown fs type of item '$item'"
			}
		}
	}
	
}
