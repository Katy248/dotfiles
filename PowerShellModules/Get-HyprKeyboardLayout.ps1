$mainKeyboard = (hyprctl devices -j | ConvertFrom-Json).keyboards | Where-Object { $_.main -eq $True }

Write-Information $mainKeyboard

return $mainKeyboard.active_keymap