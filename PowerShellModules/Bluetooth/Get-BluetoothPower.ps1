function Get-BluetoothPower {

	[CmdletBinding()]
	param (
		[switch]$ToLower = $False
	)

	$state = ((bluetoothctl show | Out-String) -split "\n")[6]

	$isPowerOn = $state -like "*yes" 

	if ($ToLower)
	{
		return "$isPowerOn".ToLower()
	}
	else 
	{
		return $isPowerOn
	}
}