function Switch-BluetoothPower {
    [CmdletBinding()]
    param ()
    
    $power = ~/.config/scripts/Get-BluetoothPower.ps1
    Write-Debug $power
    $state = [bool]::Parse($power)

    Write-Information "Current state: is bluetooth on - $state"

    if ($state) {
        bluetoothctl power off   
    }
    else {
        bluetoothctl power on   
    }
}

