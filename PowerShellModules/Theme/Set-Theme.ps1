function Set-Theme {
[CmdletBinding()]
param(
    [string]$ThemeFilePath
)

Begin {
    if (-not (Test-Path -Path $ThemeFilePath)) {
        Write-Error -Message "File '$ThemeFilePath' doesn't exists" -Category InvalidArgument -TargetObject $ThemeFilePath
        break
    }
}

Process {
    Write-Information "Start initializing theme with config: '$ThemeFilePath'"
    oh-my-posh init pwsh --config "$ThemeFilePath" | Invoke-Expression
    Write-Information "Done"
}
}