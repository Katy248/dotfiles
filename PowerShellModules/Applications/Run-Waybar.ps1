
function Run-Waybar {
    [CmdletBinding()]
    param (
        [string]$ConfigPath = "~/.themes/current/waybar/config.jsonc",
        [string]$ThemePath = "~/.themes/current/waybar/theme.css"
    )
    
    if ((Test-Path $ConfigPath) -and (Test-Path $ThemePath)) {
        waybar --config $ConfigPath --style $ThemePath
    }  
    else {
        waybar
    }
    
}




