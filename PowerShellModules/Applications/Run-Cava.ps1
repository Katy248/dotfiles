function Run-Cava {
    [CmdletBinding()]
    param (
        [Int32]$Delay=2
    )
    
    Start-Sleep $Delay
    cava
}