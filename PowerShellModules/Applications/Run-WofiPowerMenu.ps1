function Run-WofiPowerMenu {
[CmdletBinding()]
param (
        
)

$poweroff = " Poweroff"
$reboot = " Reboot"
$suspend = " Suspend"
$lock = " Lock"
$logout = " Logout"

$parameters = @($poweroff , $reboot, $suspend, $lock, $logout)
Write-Debug "Wofi parameters is '$parameters'"

$result = (Write-Output @parameters | wofi -i --dmenu | Out-String).Replace("\n", "").Trim()
Write-Debug "Wofi result is '$result'"

switch ($result) {
        $poweroff{  
                Write-Debug "Starting shutdown"
                shutdown
                $notificationResult = (notify-send "System" "Shutdown in less than 1 minute" -A "Not now") | Out-String
                if ($notificationResult -eq "0") {
                        Write-Debug "Shurdown canceling"
                        shutdown -c
                        Write-Debug "Shurdown canceled"
                }
                
        }
        $reboot {
                Write-Debug "Starting reboot"
                reboot
        }
        $suspend {
                Write-Debug "Starting suspend"
                systemctl suspend
        }
        $lock {
                Write-Debug "Starting lock"
                swaylock
        }
        $logout {
                Write-Debug "Starting logout"
                hyprctl dispatch exit 1
        }
}

# $op=

# case $op in 
#         poweroff)
#                 ;&
#         reboot)
#                 ;&
#         suspend)
#         systemctl $op
#                 ;;
#         lock)
# 		swaylock
#                 ;;
#         logout)
#         hyprctl dispatch exit 1
#         swaymsg exit
#                 ;;
# esac
}