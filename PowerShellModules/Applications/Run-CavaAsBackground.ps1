function Run-CavaAsBackground {
    [CmdletBinding()]
    param (
        [string]$Class="cava-bg",
        [string]$KittyConfigurationFile
    )
    hyprpm enable hyprwinwrap
    kitty --class="$Class" -c "$home/.config/kitty/bg.conf" "$home/Scripts/Run-Cava.ps1"
}