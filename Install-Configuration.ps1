<#
.SYNOPSIS 
	Installs configuration on machine.
.DESCRIPTION
	Creates soft copy of specified directories on computer.
.NOTES
	Information or caveats about the function e.g. 'This function is not supported in Linux'
.LINK
	https://gitlab.com/Katy248/Dotfiles
.EXAMPLE
	./Install-Configuration.ps1
	Will soft copy files that doesn't exist in destination folder.
.EXAMPLE
	./Install-Configuration.ps1 -Force
	Will soft copy all files. If such file already exists in destination folder it will be replaced.
.PARAMETER Force
	Enable force copy, if file in destination already exists it will be replaced.
#>


param (
	[switch]$Force = $false,
	[switch]$Debug = $false
)

. $PSScriptRoot/Functions.ps1

# git clone https://gh/Katy248/PowerShellModules ./PowerShellModules
# git clone https://gh/Katy248/Wallpapers ./Wallpapers


if ($IsWindows) {
	$documents = $([Environment]::GetFolderPath('MyDocuments'))
	$pictures = $([Environment]::GetFolderPath('MyPictures'))
	$modulesPath = ($env:PSModulePath).Split(";")[0]
	
	New-Link "$PSScriptRoot/PowerShellModules" "$modulesPath" -Force:$Force -Debug:$Debug
	New-Link "$PSScriptRoot/Configurations/powershell" "$documents/PowerShell" -Force:$Force -Debug:$Debug
	New-Link "$PSScriptRoot/Wallpapers" "$pictures/Wallpapers" -Force:$Force -Debug:$Debug
	New-Link "$PSScriptRoot/Configurations/helix" "$home/AppData/Roaming/helix" -Force:$Force -Debug:$Debug
	New-Link "$PSScriptRoot/Configurations/Code" "$home/AppData/Roaming/Code" -Force:$Force -Debug:$Debug
}
if ($IsLinux) {
	New-Link "$PSScriptRoot/PowerShellModules" "$home/.local/share/powershell/Modules" -Force:$Force -Debug:$Debug
	New-Link "$PSScriptRoot/Wallpapers" "$home/Pictures/Wallpapers" -Force:$Force -Debug:$Debug
	New-Link "$PSScriptRoot/GnomeExtensions" "$home/.local/share/gnome-shell/extensions" -Force:$Force -Debug:$Debug
}
New-Link "$PSScriptRoot/Configurations" "$home/.config" -Force:$Force -Debug:$Debug
New-Link "$PSScriptRoot/Scripts" "$home/Scripts" -Force:$Force -Debug:$Debug
