using ThemeManager.Cli.Models.Configuration;

namespace ThemeManagerConfigurations;

public class MyConfiguration : IConfigurationSetter<ApplicationConfiguration>
{
	public Configuration GetConfiguration() => new(
		//CurrentTheme: new()
		//{
		//	Name: "SharpBlue",
		//	Repository: null 
		//},
		"SharpBlue@katys",
		themeRepositories: new() 
		{
			{ "katys", "https://github.com/katy248/ThemesRepository" }
		}
	)
}
