$InformationPreference = 'Continue'

Import-Module -Global Bluetooth
Import-Module -Global Theme
Import-Module -Global Applications

<# Preferences for terminal #>

$isInteractive = ([System.Environment]::GetCommandLineArgs().Contains('-NonInteractive') -or [System.Environment]::GetCommandLineArgs().Contains('-noni'))

if (-not $isInteractive)
{
    if (Test-Path "~/.themes/current/powershell/theme.omp.json")
    {
        Set-Theme -ThemeFilePath "~/.themes/current/powershell/theme.omp.json"
        Start-Sleep -Seconds 0.8
        Clear-Host
    }
}

<# Windows only #>
if ($IsWindows) {
    Set-Alias lvim 'C:\Users\Katy\.local\bin\lvim.ps1'
}
else {
    Set-Alias thm '~/ThemeManager/ThemeManager.Cli/bin/Release/net8.0/thm'
}
