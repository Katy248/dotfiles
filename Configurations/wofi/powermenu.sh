op=$( echo -e " Poweroff\n Reboot\n Suspend\n Lock\n Logout" | wofi -i --dmenu | awk '{print tolower($2)}' )

case $op in 
        poweroff)
                ;&
        reboot)
                ;&
        suspend)
        systemctl $op
                ;;
        lock)
		swaylock
                ;;
        logout)
        hyprctl dispatch exit 1
        swaymsg exit
                ;;
esac
