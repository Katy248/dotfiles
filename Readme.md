# Dotfiles

My `SharpBlue` themed environment files.

## Apps

Base application:

- `hyprland` for wm
- `hyprpaper` for wallpapers
- `wofi` as app launcher
- `waybar` as statusbar
- `swaync` for notifications
- `swayide`+`swaylock` for screenlocking
- `powershell` as shell

## TODO

Nothing for now

## Folders

- ### Configurations 
    
Contains all folders and files for `.config` folder

- ### PowerShellModules 

Contains all `powershell` modules to use for automatization

- ### Wallpapers

Wallpapers